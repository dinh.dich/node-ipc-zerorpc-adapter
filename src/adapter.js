/**
 * Created by DICH on 4/15/2017.
 */
let request = require('request');
let _defaults = require('lodash').defaults;
let zerorpc = require("zerorpc");
let inherits = require('util').inherits;
let EventEmitter = require('events').EventEmitter;
let ServiceStates = require('../constants/ServiceState');

function Adapter(options) {
    options = _defaults({}, options, {
        host: 'http://localhost',
        port: 4242,
        heartbeatInterval: 5000,        //ms
        timeout: 30     //second
    })

    let self = this;
    this.host = options.host;
    this.port = options.port;

    this.client = new zerorpc.Client(options);
    this.client.on('connect', function () {
        self.connected = true
        self.state = ServiceStates.CONNECTED;
        self.emit('connected');
    })

    this.client.on('disconnect', function () {
        self.connected = false
        self.state = ServiceStates.DISCONNECT;
        self.emit('disconnected');
    })

    this.client.on('close', function () {
        self.connected = false
        self.state = ServiceStates.DISCONNECT;
        self.emit('disconnected');
    })

    this.client.on('error', function (error) {
        self.emit('error', error);
    })

    this.client.connect("tcp://" + this.host + ":" + this.port);

    this.state = ServiceStates.WAITING_CONNECT;
}

inherits(Adapter, EventEmitter);

/**
 *
 * @param method
 * @param params
 * @param {error, result} callback
 */
Adapter.prototype.callMethod = function (method, params, callback) {
    this.client.invoke(method, ...params, function (error, res, more) {
        if(error) {
            return callback(error)
        }

        if(!res) {
            return callback(null, {})
        }

        if(!res.result) {
            res = {
                result: res
            }
        }
        res.result = [...res.result];
        callback(error, res, more)
    })
}

/**
 *
 * @return {boolean}
 */
Adapter.prototype.isConnected = function () {
    return !!this.connected;
}

/**
 *
 * @return {number|*}
 */
Adapter.prototype.getState = function () {
    return this.state;
}

module.exports = Adapter;